import './App.css';
import React,{Component} from "react";

import Chat from "./containers/chat";




class App extends Component{
    constructor(props) {
        super(props);
        this.state = {
            chatId: '1',
            chatName: 'My chat',
            currentUserId: '80e03248-1b8f-11e8-9629-c7eca82aa7bd',
            currentUserName: 'Doonly',
            isClient:true
        };
    }
    render() {
         return (
            <Chat
                chatName={this.state.chatName}
                currentUserId={this.state.currentUserId}
                currentUserAvatar={this.state.currentUserAvatar}
                currentUserName={this.state.currentUserName}
                isClient = {this.state.isClient}
            />
        );
    }
}


export default App;
