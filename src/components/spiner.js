import React from 'react';
import "../styles/spinner.css";

export default class Spinner extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="spinner">
                LoadingData...
            </div>
        )
    }
}