import React from "react";
import "../styles/editor.css";

export default class EditorMessage extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        let classList = "editor-wrapper closeEditor";
        if (this.props.isOpen) {
            classList += " openEditor";
        }

        return (
            <div>
                <div className={classList}>
                <textarea
                    className="message-fill"
                    id="textEditData"
                    value={this.props.messageText}
                    onChange={this.props.changeNewMessage}
                />
                    <div>
                        <input type="button" id="sendEditMessage" onClick={this.props.sendEditMessage} value="Ok"/>
                        <input type="button" id="closeEditWindow" value="Cancel" onClick={this.props.isClose}/>
                    </div>
                </div>

            </div>
        )
    }
}
