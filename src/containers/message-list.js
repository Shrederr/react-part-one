import React from "react";
import "../styles/message-list.css";
import Message from "../components/message";

export default class MessageList extends React.Component {
    constructor(props) {
        super(props);
    }

    isLike = (id) => {
        return this.props.likeList.includes(id);
    }

    render() {
        return (

            <div className="message-list">
                {
                    this.props.messageList.map((user) => {
                        return <Message text={user.text}
                                        avatar={user.avatar}
                                        id={user.id}
                                        name={user.user}
                                        key={user.id}
                                        likeMessage={this.props.likeMessage.bind(null, user.id)}
                                        createdAt={new Date(user.createdAt).toLocaleString()}
                                        isClient={user.isClient}
                                        isLike={this.isLike(user.id)}
                                        deleteMessage={this.props.deleteMessage.bind(null, user.id)}
                                        openEditor={this.props.editMessage.bind(null, user.id)}

                        />
                    })
                }
            </div>
        )
    }
}

