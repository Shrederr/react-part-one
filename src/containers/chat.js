import React, {useEffect, useState} from 'react';
import Header from "./header";
import MessageList from "./message-list";
import MessageInput from "./input";
import uuid from 'react-uuid';
import EditorMessage from "../components/editModal";
import Spinner from "../components/spiner";

const Chat = ({chatName, currentUserId, currentUserName, currentUserAvatar, isClient}) => {
    const API_LINK = 'https://edikdolynskyi.github.io/react_sources/messages.json';
    const [state, setState] = useState({
        userCounts: 20,
        messageCounts: 0,
        lastMessageDate: "2020-07-16T19:48:42.481Z",
        messageList: []
    })

    const [message, setMessage] = useState('');
    const [spinner, setSpinner] = useState({spinnerLoad: true});
    const [newEditMessage, setNewEditMessage] = useState([])
    const [editMessage, setNewMessage] = useState({
        isOpen: false,
        id: ""
    });
    const [likes, setLike] = useState([]);
    const fetchData = async () => {
        return fetch(API_LINK).then((response) => response.json());
    };

    function sendMessage() {
        if (!message.replace(/\s+/g, '')) {
            return;
        }
        const date = new Date().toISOString();
        const newMessage = {
            id: uuid(),
            userId: currentUserId,
            avatar: currentUserAvatar,
            user: currentUserName,
            text: message,
            createdAt: date,
            editedAt: date,
            isClient: isClient
        };
        setState((s) => ({
            messageList: [...s.messageList, newMessage],
            lastMessageDate: date,
            messageCounts: state.messageList.length + 1,
            userCounts: state.userCounts
        }));

        setMessage('');
    }

    function sendEditMessage() {
        const index = state.messageList.findIndex((item) => {
            return item.id === editMessage.id
        });
        const editDate = new Date().toISOString();

        setState((s) => {
            const newList = [...s.messageList];
            newList[index].text = newEditMessage;
            newList[index].editedAt = editDate;
            return {
                ...s,
                messageList: newList,

            }
        })
        closeEditor();

    }


    function changeMessage(e) {
        setMessage(e.target.value);
    }

    function changeNewMessage(e) {
        setNewEditMessage(e.target.value);
    }

    const deleteMessage = (id) => {
        const index = state.messageList.findIndex((item) => {
            return item.id === id
        });
        setState((s) => {
            const newList = [...s.messageList];
            const newDate = s.messageList[index - 1].createdAt;
            const newCountsMessage = s.messageCounts - 1;
            newList.splice(index, 1);
            return {
                ...s,
                messageList: newList,
                userCounts: 20,
                lastMessageDate: newDate,
                messageCounts: newCountsMessage
            }
        })
    }
    const isLike = (id) => {
        const index = likes.indexOf(id);
        if (index === -1) {
            setLike((prevLikes) => [...prevLikes, id]);
        } else {
            setLike((prevLikes) => {
                const newLikes = [...prevLikes]
                newLikes.splice(index, 1);
                return newLikes;
            });
        }
    };
    const openEditor = (id) => {
        setNewMessage((s) => {
            const status = true;
            return {
                ...s,
                isOpen: status,
                id: id
            }
        })
    }
    const closeEditor = () => {
        setNewMessage((s) => {
            const status = false;
            return {
                ...s,
                isOpen: status,
                id: ""
            }
        })
        setNewEditMessage('');
    }

    function sortByDate(arr) {
        arr.sort((a, b) => new Date(a.createdAt).getTime() > new Date(b.createdAt).getTime() ? 1 : -1);
    }

    useEffect(() => {
        const _fetchData = async () => {
            const messageList = await fetchData();
            sortByDate(messageList);
            setState((s) => ({
                ...s,
                messageList,
                messageCounts: messageList.length
            }));
            setSpinner((s) => {
                return {
                    ...s,
                    spinnerLoad: false
                }
            })
        };
        _fetchData();
    }, []);


    if (!spinner.spinnerLoad) {
        return (
            <div>
                <Header
                    chatName={chatName}
                    usersCounts={state.userCounts}
                    messagesCounts={state.messageCounts}
                    lastMessageDate={state.lastMessageDate}
                />
                <MessageList messageList={state.messageList}
                             currentUserId={currentUserId} likeMessage={isLike}
                             likeList={likes} deleteMessage={deleteMessage} editMessage={openEditor}/>
                <EditorMessage isOpen={editMessage.isOpen} isClose={closeEditor} messageText={newEditMessage}
                               changeNewMessage={changeNewMessage}
                               sendEditMessage={sendEditMessage}/>
                <MessageInput sendMessage={sendMessage} changeMessage={changeMessage} message={message}/>
            </div>
        )
    } else {
        return (
            <Spinner/>
        )
    }
}
export default Chat;
